import React, { Component } from 'react';
import { connect } from 'dva';
import { Button, Input, TreeSelect, Spin, Modal, Row, Col } from 'antd';
import router from 'umi/router';

import { createTreeSelect } from '@/components/TreeSelects';
import ModalBar from '@/components/ModalBar';
import styles from './$id.less';

const { TextArea } = Input;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

// 修改角色Page
@connect(({ loading, roleActions }) => ({
  getRoleInfoLoading: loading.effects['roleActions/getRoleInfo'],
  updateRoleInfoLoading: loading.effects['roleActions/updateRoleInfo'],
  roleInfo: roleActions.roleInfo,
  resourceList: roleActions.resourceList,
  oriSelectedKeys: roleActions.oriSelectedKeys,
  showModal: roleActions.showModal,
}))
class EditRole extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      areaText: '',
    });
  }

  componentWillMount() {
    const { dispatch, match } = this.props;
    dispatch({
      type: 'roleActions/getRoleInfo',
      data: {
        roleId: match.params.id,
      },
    });
  }

  textAreaChange = (e) => {
    this.setState({
      areaText: e.target.value,
    });
  };

  handleSubmit = () => {
    const { dispatch, oriSelectedKeys, roleInfo } = this.props;
    dispatch({
      type: 'roleActions/updateRoleInfo',
      data: {
        ...roleInfo,
        resourceList: oriSelectedKeys,
      },
    });
  };

  toBack = () => {
    router.goBack();
  };

  onChange = (value) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'roleActions/changeSelectedKey',
      oriSelectedKeys: value,
    });
  };

  // model确认
  handleOk = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'roleActions/closeModel',
    });
    this.toBack();
  };

  render() {
    const { getRoleInfoLoading, roleInfo, resourceList, oriSelectedKeys, showModal, updateRoleInfoLoading } = this.props;

    let role = {
      roleName: '',
    };
    if (roleInfo !== '' && roleInfo !== undefined) {
      role = roleInfo;
    }

    const treeProps = {
      value: oriSelectedKeys,
      onChange: this.onChange,
      treeCheckable: true,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: '请选择权限',
      style: {
        width: '100%',
      },
    };

    let treeSelect = createTreeSelect(treeProps, resourceList !== undefined ? resourceList : []);

    return <div className={styles.main}>
      <Spin
        tip={'正在加载...'}
        spinning={getRoleInfoLoading}
      >
        <div className={styles.form}>
          <Row className={styles.item}>
            <Col span={5} className={styles.col}>
              <label>角色名：</label>
            </Col>
            <Col span={18} offset={1}>
              <Input className={styles.input} value={role.roleName} disabled={true}/>
            </Col>
          </Row>
          <Row className={styles.item}>
            <Col span={5} className={styles.col}>
              <label>权限：</label>
            </Col>
            <Col span={18} offset={1}>
              {treeSelect}
            </Col>
          </Row>
          <Row className={styles.item}>
            <Col span={5} className={styles.col}>
              <label>备注：</label>
            </Col>
            <Col span={18} offset={1}>
              <TextArea className={styles.text} onChange={this.textAreaChange}/>
            </Col>
          </Row>
        </div>
        <div className={styles.btn_group}>
          <Button type="primary" style={{ marginRight: '121px' }} onClick={this.handleSubmit}
                  loading={updateRoleInfoLoading}>提交</Button>
          <Button onClick={this.toBack}>返回</Button>
        </div>
      </Spin>
      <ModalBar loading={showModal} title={'成功'} backText={'返回'} text={'角色修改成功'} handleOk={this.handleOk}/>
    </div>;
  }
}

export default EditRole;
