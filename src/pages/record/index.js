import React, { Component } from 'react';
import { connect } from 'dva';

import { createBreadcrumbs } from '@/components/Breakcrumbs';
import { createTable } from '@/components/Tables';
import RecordSearchBar from './components/RecordSearchBar';
import RecordTableColumnBar from './components/RecordTableColumnBar';
import styles from './index.less';

// 录音Page
@connect(({ loading, recordActions }) => ({
  recordList: recordActions.recordList,
  getRecordListLoading: loading.effects['recordActions/getRecordList'],
}))
class RecordList extends Component {

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'recordActions/getRecordList',
    });
  }

  render() {
    const { recordList, getRecordListLoading } = this.props;

    let column = RecordTableColumnBar(this.props);
    let Breadcrumb = createBreadcrumbs();
    let TableComponent = createTable(column, recordList, 'callId', getRecordListLoading);
    return (
      <div>
        <Breadcrumb/>
        <div className={styles.main}>
          <RecordSearchBar {...this.props}/>
          {TableComponent}
        </div>
      </div>
    );
  }
}

export default RecordList;
