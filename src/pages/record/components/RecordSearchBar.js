import React, { Component } from 'react';
import { Button, Input } from 'antd';

import styles from './RecordSearchBar.less';

// 搜索组件
class RecordSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      filterName: '',
      filterId: '',
    });
  }

  handleNameChange = (e) => {
    this.setState({
      filterName: e.target.value,
    });
  };

  handleIdChange = (e) => {
    this.setState({
      filterId: e.target.value,
    });
  };

  // 筛选角色列表
  filterRecord = () => {
    const { dispatch } = this.props;
    const { filterName, filterId } = this.state;
    dispatch({
      type: 'recordActions/getRecordList',
      data: {
        taskName: filterName,
        customerId: filterId
      },
    });
  };

  // 重置
  resetFilter = () => {
    const { dispatch } = this.props;
    this.setState({
      filterName: '',
      filterId: '',
    });
    dispatch({
      type: 'recordActions/getRecordList',
    });
  };

  render() {
    const { filterName, filterId } = this.state;

    return <div className={styles.main}>
      <span>任务名称：</span>
      <Input placeholder={'请输入关键字'} value={filterName} onChange={this.handleNameChange}/>
      <span>客户ID：</span>
      <Input placeholder={'请输入客户Id'} value={filterId} onChange={this.handleIdChange}/>
      <Button className={styles.btn} type="primary" onClick={this.filterRecord}>查询</Button>
      <Button onClick={this.resetFilter}>重置</Button>
    </div>;
  }
}

export default RecordSearchBar;
