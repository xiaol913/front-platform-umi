import React from 'react';
import { Divider } from 'antd';

// 列表表头组件
export default () => {
  const playRecord = (key, id) => {
    let audios = document.getElementsByTagName('audio');
    for (let i = 0; i < audios.length; i++) {
      audios[i].pause();
    }
    let audio = document.getElementById(id);
    switch (key) {
      case 'play':
        audio.play();
        break;
      case 'pause':
        audio.pause();
        break;
      default:
        break;
    }
  };

  // const MoreBtn = props => (
  //   <Dropdown
  //     overlay={
  //       <Menu onClick={(key) => playRecord(key, props.current)}>
  //         <Menu.Item key="play">播放</Menu.Item>
  //         <Menu.Item key="pause">暂停</Menu.Item>
  //       </Menu>
  //     }
  //   >
  //     <a>
  //       更多 <Icon type="down"/>
  //     </a>
  //   </Dropdown>
  // );

  return [
    {
      title: 'ID',
      dataIndex: 'callId',
      key: 'callId',
    },
    {
      title: '任务名称',
      dataIndex: 'taskName',
      key: 'taskName',
    },
    {
      title: '客户ID',
      dataIndex: 'customerId',
      key: 'customerId',
    },
    {
      title: '录音长度',
      dataIndex: 'callTime',
      key: 'callTime',
    },
    {
      title: '录音时间',
      dataIndex: 'createTime',
      key: 'createTime',
    },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => {
        return <span>
      <a href={`${record.recordFile}`}>下载</a>
      <Divider type="vertical"/>
        <audio id={record.callId} src={record.recordFile} controls={false} preload="none" controlsList="nodownload">
                      <track kind="captions"/>
                      您的浏览器不支持 audio 元素。
                    </audio>
      <a href="javascript: void(0);" onClick={() => playRecord('play', record.callId)}>播放</a>
      <Divider type="vertical"/>
      <a href="javascript: void(0);" onClick={() => playRecord('pause', record.callId)}>暂停</a>
          {/*<MoreBtn {...props} current={record.callId}/>*/}
    </span>;
      },
    }];
}
