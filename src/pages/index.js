import React from 'react';

import styles from './index.less';

// 默认主页面
export default function() {
  return (
    <div className={styles.main}>
      <p>智能外呼业务平台前置系统</p>
    </div>
  );
}
