import React, { Component } from 'react';
import { connect } from 'dva';
import { Tabs } from 'antd';

import FormTabItemBar from '@/pages/form/components/FormTabItemBar';
import { TaskSummaryColumn, CallSummaryColumn } from '@/pages/form/components/SummaryColumnBar';
import FormSearchBar from '@/pages/form/components/FormSearchBar';
import styles from './index.less';

const TabPane = Tabs.TabPane;

// 搜索栏参数
const TaskSearchProps = {
  type: 'task',
  radio: {
    status: false,
    list: [],
  },
  search: {
    status: true,
    label: '任务名称',
  },
};

const CallSearchProps = {
  type: 'call',
  radio: {
    status: true,
    list: [
      {
        value: 'day',
        text: '日报表',
      },
    ],
  },
  search: {
    status: false,
    label: '',
  },
};

// 报表Page
@connect(({ loading, formActions }) => ({
  getSummaryFormLoading: loading.effects['formActions/getSummaryForm'],
  summaryTaskList: formActions.summaryTaskList,
  summaryCallList: formActions.summaryCallList,
}))
class SummaryFormList extends Component {

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'formActions/getSummaryForm',
    });
  }

  render() {
    const { summaryTaskList, summaryCallList, getSummaryFormLoading } = this.props;

    return <div className={styles.main}>
      <Tabs defaultActiveKey={'0'}>
        <TabPane tab={'任务汇总'} key={'0'}>
          <FormSearchBar {...this.props} SearchBarProps={TaskSearchProps}/>
          <FormTabItemBar loading={getSummaryFormLoading} column={TaskSummaryColumn} data={summaryTaskList} keyId={'uuid'}/>
        </TabPane>
        <TabPane tab={'外呼汇总'} key={'1'}>
          <FormSearchBar {...this.props} SearchBarProps={CallSearchProps}/>
          <FormTabItemBar loading={getSummaryFormLoading} column={CallSummaryColumn} data={summaryCallList} keyId={'uuid'}/>
        </TabPane>
      </Tabs>
    </div>;
  }
}

export default SummaryFormList;
