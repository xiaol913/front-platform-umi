import React, { Component } from 'react';
import { Tabs } from 'antd';
import ReactEcharts from 'echarts-for-react';

import { isEmptyObject } from '@/utils/common';
import FormTabItemBar from '@/pages/form/components/FormTabItemBar';
import FormChartOptionBar from '@/pages/form/components/FormChartOptionBar';
import FormSearchBar from '@/pages/form/components/FormSearchBar';
import styles from './index.less';
import test_data from '@/pages/form/test_data';

const TabPane = Tabs.TabPane;

const SearchBarProps = {
  type: 'other',
  radio: {
    status: true,
    list: [
      {
        value: 'week',
        text: '周报表',
      },
      {
        value: 'month',
        text: '月报表',
      },
      {
        value: 'session',
        text: '季报表',
      },
      {
        value: 'year',
        text: '年报表',
      },
    ],
  },
  search: {
    status: false,
    label: '',
  },
};


// 报表Page
class BusinessFormList extends Component {

  render() {

    return <div className={styles.main}>
      {test_data.form_info.length > 0 &&
      <Tabs defaultActiveKey={test_data.form_info[0].id}>
        {test_data.form_info.map((item, index) => (
          <TabPane tab={item.title} key={item.id}>
            <FormSearchBar {...this.props} SearchBarProps={SearchBarProps}/>
            <FormTabItemBar loading={false} column={item.header} data={item.data} keyId={item.row_key}/>
            {isEmptyObject(item.option) &&
            <ReactEcharts
              option={FormChartOptionBar(item.option)}
              notMerge={true}
              lazyUpdate={true}
              // onEvents={onEvents}
              style={{ width: '100%', height: 400, marginTop: 50 }}
            />
            }
          </TabPane>
        ))}
      </Tabs>
      }
    </div>;
  }
}

export default BusinessFormList;
