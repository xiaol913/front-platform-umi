import React, { Component } from 'react';
import { Button, Radio, DatePicker, Input } from 'antd';

import styles from './FormSearchBar.less';

const { RangePicker } = DatePicker;
const Search = Input.Search;

// 搜索组件
class FormSearchBar extends Component {
  constructor(props) {
    super(props);
    const { SearchBarProps } = props;
    this.state = ({
      formType: SearchBarProps.type,
      date: [],
      dateString: [],
      filterValue: SearchBarProps.radio.status ? SearchBarProps.radio.list[0].value : '',
      searchValue: '',
    });
  }

  handleRadioChange = (e) => {
    this.setState({ filterValue: e.target.value });
  };

  handlePickerChange = (date, dateString) => {
    this.setState({ date, dateString });
  };

  // 查询
  handleSearch = () => {
    const { dispatch } = this.props;
    const { dateString, formType, searchValue } = this.state;
    dispatch({
      type: 'formActions/getSummaryForm',
      data: {
        dateString,
        formType,
        searchValue
      },
    });
  };

  handleInputChange = (e) => {
    this.setState({
      searchValue: e.target.value,
    });
  };

  // 重置
  resetFilter = () => {
    const { dispatch, SearchBarProps } = this.props;
    this.setState({
      searchValue: '',
      date: [],
      filterValue: SearchBarProps.radio.status ? SearchBarProps.radio.list[0].value : '',
    });
    dispatch({
      type: 'formActions/getSummaryForm',
    });
  };

  render() {
    const { filterValue, date, searchValue } = this.state;
    const { SearchBarProps } = this.props;

    return <div className={styles.main}>
      <RangePicker
        value={date}
        onChange={this.handlePickerChange}
        className={styles.btn}
      />
      {SearchBarProps.search.status &&
      <Search
        className={styles.search}
        placeholder={`请输入关键字：${SearchBarProps.search.label}`}
        onChange={this.handleInputChange}
        value={searchValue}
        onSearch={this.handleSearch}
      />
      }
      {SearchBarProps.radio.status &&
      <Radio.Group
        value={filterValue}
        onChange={this.handleRadioChange}
        className={styles.btn}
      >
        {SearchBarProps.radio.list.map((item, index) => (
          <Radio.Button key={index} value={item.value}>{item.text}</Radio.Button>
        ))}
      </Radio.Group>
      }
      <Button className={styles.btn} type="primary" onClick={this.handleSearch}>查询</Button>
      <Button className={styles.btn} onClick={this.resetFilter}>重置</Button>
    </div>;
  }
}

export default FormSearchBar;
