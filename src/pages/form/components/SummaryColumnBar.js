export const TaskSummaryColumn = [
  {
    title: '任务名称',
    dataIndex: 'taskName',
    key: 'taskName',
  },
  {
    title: '状态',
    key: 'status',
    render: (text, record) => {
      switch (record.status) {
        case 0:
          return '未开始';
        case 1:
          return '进行中';
        case 2:
          return '已完成';
        default:
          return '未知';
      }
    },
  },
  {
    title: '开始时间',
    dataIndex: 'startTime',
    key: 'startTime',
  },
  {
    title: '结束时间',
    dataIndex: 'endTime',
    key: 'endTime',
  },
  {
    title: '名单数',
    dataIndex: 'rosterNum',
    key: 'rosterNum',
  },
  {
    title: '外呼次数',
    dataIndex: 'outCallNum',
    key: 'outCallNum',
  },
  {
    title: '完成率',
    key: 'completeRate',
    render: (text, record) => {
      return record.completeRate * 100 + '%';
    },
  },
  {
    title: '应答率',
    key: 'answerRate',
    render: (text, record) => {
      return record.answerRate * 100 + '%';
    },
  },
  {
    title: '有效应答数',
    dataIndex: 'effectiveNum',
    key: 'effectiveNum',
  },
];

export const CallSummaryColumn = [
  {
    title: '日期',
    dataIndex: 'dateTime',
    key: 'dateTime',
  },
  {
    title: '完成任务数',
    dataIndex: 'completeTaskNum',
    key: 'completeTaskNum',
  },
  {
    title: '外呼次数',
    dataIndex: 'outcallNum',
    key: 'outcallNum',
  },
  {
    title: '外呼成功次数',
    dataIndex: 'outcallSuccessNum',
    key: 'outcallSuccessNum',
  },
  {
    title: '应答次数',
    dataIndex: 'outcallAnswerNum',
    key: 'outcallAnswerNum',
  },
  {
    title: '总通话时长',
    dataIndex: 'totalOutcallTime',
    key: 'totalOutcallTime',
  },
  {
    title: '平均通话时长',
    dataIndex: 'avgOutcallTime',
    key: 'avgOutcallTime',
  },
];
