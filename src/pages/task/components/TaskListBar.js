import React, { Component } from 'react';
import { Button, Row, Col, Progress, Menu, List, Divider, Dropdown, Icon, Spin } from 'antd';
import router from 'umi/router';

import { getDateTime } from '@/utils/common';
import { PAGE_SIZE } from '@/utils/constant';
import styles from './TaskListBar.less';

// 审核进度条组件
const ProgressItem = (data) => {
  // 审核状态 -1.审核不通过 0.待审核 1.主动撤回 2.审核中 3.审核通过
  let item = {
    status: 'normal',
    text: '无法识别',
    percent: 0,
  };

  switch (data.status) {
    case -1:
      item = {
        status: 'exception',
        text: '审核不通过',
        percent: 100,
      };
      break;
    case 0:
      item = {
        status: 'normal',
        text: '待审核',
        percent: 0,
      };
      break;
    case 1:
      item = {
        status: 'exception',
        text: '主动撤回',
        percent: 0,
      };
      break;
    case 2:
      item = {
        status: 'active',
        text: '审核中',
        percent: 50,
      };
      break;
    case 3:
      item = {
        status: 'success',
        text: '审核通过',
        percent: 100,
      };
      break;
    default:
      item = {
        status: 'normal',
        text: '无法识别',
        percent: 0,
      };
  }

  return <div className={styles.progress}>
    <Progress className={styles.length} showInfo={false} percent={item.percent} size="small" status={item.status}/><span
    className={styles.text}>{item.text}</span>
  </div>;
};

// 更多
const MoreBtn = props => (
  <Dropdown
    overlay={
      <Menu onClick={() => {
        const { dispatch } = props;
        dispatch({
          type: 'taskActions',
        });
      }}>
        <Menu.Item key="delete">删除</Menu.Item>
      </Menu>
    }
  >
    <a>
      更多 <Icon type="down"/>
    </a>
  </Dropdown>
);

// 图标
const taskIcon = (type) => {
  let color = '';
  let text = '';
  switch (type) {
    case 'CS_TEST':
      color = '#597EF7';
      text = '催收';
      break;
    case 'YX_TEST':
      color = '#36CFC9';
      text = '营销';
      break;
    default:
      color = '#4af721';
      text = '其他';
      break;
  }
  return <Col span={2} className={styles.icon} style={{ backgroundColor: color }}>
    <span>{text}</span>
  </Col>;
};

// 单个列表组件
class ListItem extends Component {

  // 审核
  auditTask = (id, opType) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'taskActions/auditTask',
      data: {
        opType,
        taskId: id,
      },
    });
  };

  render() {
    const { item } = this.props;
    return <Row className={styles.item}>
      {taskIcon(item.taskType)}
      <Col span={8} className={styles.desc}>
        <p className={styles.title}>
          {item.taskName}
        </p>
        <p className={styles.des}>编号：{item.taskNo}</p>
      </Col>
      <Col span={2} className={styles.common}>
        <p className={styles.first}>发起人</p>
        <p className={styles.second}>{item.createOper}</p>
      </Col>
      <Col span={4} className={styles.common}>
        <p className={styles.first}>创建时间</p>
        <p className={styles.second}>{getDateTime(item.createTime)}</p>
      </Col>
      <Col span={6} className={styles.common}>
        <p className={styles.first}>审核进度</p>
        <ProgressItem status={item.auditStatus}/>
      </Col>
      <Col span={2} className={styles.action}>
        {item.auditStatus < 3 && item.auditStatus >= 0 ?
          <div>
            <a href="javascript:void(0);" onClick={() => this.auditTask(item.taskId, 1)}>审核</a>
            <Divider type="vertical"/>
            <a href="javascript:void(0);" onClick={() => this.auditTask(item.taskId, 0)}>驳回</a>
          </div>
          :
          <a href="javascript:void(0);">已完成</a>
        }
        {/*<a href={`/task/${item.taskId}`}>查看</a>*/}
        {/*<a href="javascript:void(0);">查看</a>*/}
        {/*<Divider type="vertical"/>*/}
        {/*<MoreBtn {...this.props} current={this.props.item.taskId}/>*/}
      </Col>
    </Row>;
  }
}

// 任务列表列表组件
class TaskListBar extends Component {

  // 跳转新增页面
  addNewTask = () => {
    router.push('/task/add');
  };

  render() {
    const page_size = PAGE_SIZE;
    const { loading, taskList } = this.props;
    // const { contentHeight } = this.state;

    return <div className={styles.main}>
      <Button className={styles.add_btn} type="dashed" block onClick={this.addNewTask}>+ 添加</Button>
      <Spin
        tip={'正在加载...'}
        spinning={loading}
      >
        <List
          itemLayout="vertical"
          pagination={{
            pageSize: page_size,
          }}
          dataSource={taskList}
          renderItem={item => (
            <ListItem {...this.props} item={item}/>
          )}
        />
      </Spin>
    </div>;
  }
}

export default TaskListBar;
