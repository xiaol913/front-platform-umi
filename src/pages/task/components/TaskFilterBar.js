import React, { Component } from 'react';
import { Radio, Input } from 'antd';

import styles from './TaskFilterBar.less';

const Search = Input.Search;

// 任务列表筛选组件
export default class TaskFilterBar extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      filterValue: 'all',
    });
  }

  handleRadioChange = (e) => {
    const radioValue = e.target.value;
    const { dispatch } = this.props;
    this.setState({
      filterValue: radioValue,
    });
    switch (radioValue) {
      case 'auditing':
        dispatch({
          type: 'taskActions/getTaskList',
          data: 'auditStatus=2',
        });
        break;
      case 'waiting':
        dispatch({
          type: 'taskActions/getTaskList',
          data: 'auditStatus=0',
        });
        break;
      default:
        dispatch({
          type: 'taskActions/getTaskList',
        });
        break;
    }
  };

  render() {
    const { filterValue } = this.state;
    const { dispatch } = this.props;

    return (<div className={styles.main}>
        <Radio.Group
          value={filterValue}
          onChange={this.handleRadioChange}
          className={styles.btn}
        >
          <Radio.Button value="all">全部</Radio.Button>
          {/*<Radio.Button value="auditing">审核中</Radio.Button>*/}
          <Radio.Button value="waiting">待审核</Radio.Button>
        </Radio.Group>
        <Search
          className={styles.search}
          placeholder="请输入关键字：任务名称"
          onSearch={value => {
            dispatch({
              type: 'taskActions/getTaskList',
              data: `taskNameLike=${value}`,
            });
          }}
        />
      </div>
    );
  }
}
