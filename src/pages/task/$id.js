import React from 'react';
import { Button } from 'antd';
import router from 'umi/router';

import { createStepBar } from '@/components/StepBar';
import styles from './$id.less';

// 模拟数据
const data = {
  'auditStatus': 3,
  'auditTemplateId': 0,
  'createOper': 'zhangsan',
  'createOperId': 1,
  'createTime': 1550275200000,
  'remark': '123',
  'taskEndTime': null,
  'taskId': 1,
  'taskNo': '20190216491726_TEST',
  'taskStartTime': null,
  'taskStatus': 1,
  'taskType': 'TEST',
  'validCount': 1,
  'validFlag': 1,
};

const process = {
  'opPermission': true,
  'processId': 0,
  'remark': '',
  'status': 0,
  'step': 0,
  'taskId': 1,
  'updateOper': '',
  'updateOperId': 0,
  'updateTime': '',
};

// 任务详情页面
export default (props) => {
  let StepBar = createStepBar(data, 'task');

  // 返回事件
  const toBack = () => {
    router.goBack();
  };

  return <div className={styles.main}>
    <div className={styles.head}>
      <div>
        <div className={styles.task_first}>
          <span>任务名称：</span><span>这个任务的名称</span>
          <div className={styles.first_btn}>
            <Button onClick={toBack}>返 回</Button>
            <Button type={'primary'}>驳 回</Button>
            <Button type={'primary'}>通 过</Button>
          </div>
        </div>
        <div className={styles.task_second}>
          <div>
            <p>
              <span>创建人：</span><span>什么丽</span>
            </p>
            <p>
              <span>业务类型：</span><span>M1</span>
            </p>
            <p>
              <span>创建时间：</span><span>2018-01-01 16:00</span>
            </p>
          </div>
          <div>
            <p>状态</p>
            <p>正在审核</p>
          </div>
        </div>
      </div>
      <div className={styles.task_third}>
        <p>流程</p>
      </div>
    </div>
    <div className={styles.progress}>
      <div className={styles.pro_title}>
        <p>审核进度</p>
      </div>
      <div>
        {StepBar}
      </div>
    </div>
  </div>;
}
