import React, { Component } from 'react';
import { connect } from 'dva';

import TaskCountBar from './components/TaskCountBar';
import TaskFilterBar from './components/TaskFilterBar';
import TaskListBar from './components/TaskListBar';
import styles from './index.less';

// 任务管理默认界面
@connect(({ loading, taskActions }) => ({
  getTaskListLoading: loading.effects['taskActions/getTaskList'],
  taskList: taskActions.taskList,
  totalCount: taskActions.totalCount,
  totalCompleted: taskActions.totalCompleted,
}))
class TaskList extends Component {

  constructor(props) {
    super(props);
    const { dispatch } = props;
    dispatch({
      type: 'taskActions/getTaskList',
    });
  }

  render() {
    const { getTaskListLoading, taskList, totalCount, dispatch, totalCompleted } = this.props;
    return <div className={styles.content}>
      <div className={styles.head}>
        <TaskCountBar totalCount={totalCount} totalCompleted={totalCompleted}/>
      </div>
      <div className={styles.form}>
        <TaskFilterBar {...this.props}/>
        <TaskListBar dispatch={dispatch} loading={getTaskListLoading} taskList={taskList}/>
      </div>
    </div>;
  }
}

export default TaskList;
