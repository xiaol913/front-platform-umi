import React, { Component } from 'react';
import { connect } from 'dva';
import { Button, Select, Upload, Icon, Form, Spin, Input } from 'antd';
import router from 'umi/router';

import { EXCEL_MODEL, FORM_ITEM_LAYOUT, FORM_BTN_LAYOUT } from '@/utils/constant';
import ModalBar from '@/components/ModalBar';
import styles from './index.less';

const Option = Select.Option;

// 新增任务Page
@connect(({ loading, taskActions }) => ({
  addPageLoading: loading.effects['taskActions/loadAddPage'],
  addTaskLoading: loading.effects['taskActions/addTask'],
  taskTypeList: taskActions.taskTypeList,
  showModal: taskActions.showModal,
}))
@Form.create()
class AddTask extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      fileList: [],
    });
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'taskActions/loadAddPage',
    });
  }

  // model确认
  handleOk = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'taskActions/closeModel',
    });
    this.toBack();
  };

  toBack = () => {
    router.push('/task');
  };

  downloadModel = () => {
    window.open(EXCEL_MODEL);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { dispatch } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { fileList } = this.state;
        dispatch({
          type: 'taskActions/addTask',
          data: {
            roll: fileList[0],
            taskType: values.taskType,
            taskName: values.taskName,
          },
        });
      } else {
        console.log('输入有误');
      }
    });
  };

  render() {
    const { fileList } = this.state;
    const { addPageLoading, addTaskLoading, taskTypeList, showModal } = this.props;
    const { getFieldDecorator } = this.props.form;
    const upProps = {
      onRemove: () => {
        this.setState({
          fileList: [],
        });
      },
      beforeUpload: (file) => {
        this.setState({
          fileList: [file],
        });
        return false;
      },
      fileList,
      // showUploadList: false,
    };
    let typeList = taskTypeList !== undefined ? taskTypeList : [];

    return <div className={styles.main}>
      <Spin
        tip={'正在加载...'}
        spinning={addPageLoading}
      >
        <Form
          onSubmit={this.handleSubmit}
          className={styles.form}>
          <Form.Item
            {...FORM_ITEM_LAYOUT}
            label={'任务名称：'}
          >
            {getFieldDecorator('taskName', {
              rules: [{ required: true, message: '请输任务名称!' }],
            })(
              <Input placeholder="请输入任务名称"/>,
            )}
          </Form.Item>
          <Form.Item
            {...FORM_ITEM_LAYOUT}
            label={'业务类型：'}
          >
            {getFieldDecorator('taskType', {
              rules: [{ required: true, message: '请选择任务类型!' }],
            })(
              <Select
              >
                {typeList.map((item, index) => (
                  <Option value={item.code} key={index}>{item.taskTypeName}</Option>
                ))}
              </Select>,
            )}
          </Form.Item>
          <Form.Item
            {...FORM_ITEM_LAYOUT}
            label={'下载模板：'}
          >
            <Button onClick={this.downloadModel} shape="round">下载</Button>
          </Form.Item>
          <Form.Item
            {...FORM_ITEM_LAYOUT}
            label={'业务参数：'}
          >
            {getFieldDecorator('undefined', {
              rules: [{ required: true, message: '请选择业务参数!' }],
            })(
              <Select
              >
                <Option value={0} key={0}>M1</Option>
                <Option value={1} key={1}>M2</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item
            {...FORM_ITEM_LAYOUT}
            label={'上传文件：'}
          >
            {getFieldDecorator('roll', {
              rules: [{ required: true, message: '请上传文件!' }],
            })(
              <Upload {...upProps}
              >
                <Button shape="round">
                  <Icon type="upload"/> 上传文件
                </Button>
              </Upload>,
            )}
          </Form.Item>
          <Form.Item
            {...FORM_BTN_LAYOUT}
            className={styles.btn_group}
          >
            <Button type="primary" htmlType={'submit'} style={{ marginRight: 50 }} loading={addTaskLoading}>
              提 交
            </Button>
            <Button onClick={this.toBack}>返回</Button>
          </Form.Item>
        </Form>
      </Spin>
      <ModalBar loading={showModal} title={'成功'} backText={'返回'} text={'新建任务成功！'} handleOk={this.handleOk}/>
    </div>;
  }
}

export default AddTask;
