import { server } from '@/utils/axios';
import { LOAD_SIZE, PAGE_NUM } from '@/utils/constant';

/**
 * 获取录音文件接口
 * @returns {AxiosPromise<any>}
 */
export function doGetRecordList(data) {
  return server.post('report/recordList', {
    domain: 'demo.cn',
    pageNum: PAGE_NUM,
    pageSize: LOAD_SIZE,
    ...data,
  });
}
