import { server } from '@/utils/axios';
import { LOAD_SIZE, PAGE_NUM } from '@/utils/constant';

/**
 * 获取任务汇总报表接口
 * @returns {AxiosPromise<any>}
 */
export function doGetSummaryTaskList(data) {
  return server.post('report/taskList', {
    domain: 'demo.cn',
    pageNum: PAGE_NUM - 1,
    pageSize: LOAD_SIZE,
    ...data
  });
}

/**
 * 获取外呼汇总报表接口
 * @returns {AxiosPromise<any>}
 */
export function doGetSummaryCallList(data) {
  return server.post('report/callDayList', {
    domain: 'demo.cn',
    pageNum: PAGE_NUM - 1,
    pageSize: LOAD_SIZE,
    ...data
  });
}
