import { server } from '@/utils/axios';
import { LOAD_SIZE, PAGE_NUM } from '@/utils/constant';

/**
 * 获取任务列表接口
 * @returns {AxiosPromise<any>}
 */
export function doGetTaskList(data) {
  if (data) {
    return server.get(`task/taskList?pageNum=${PAGE_NUM}&pageSize=${LOAD_SIZE}&${data}`);
  } else {
    return server.get(`task/taskList?pageNum=${PAGE_NUM}&pageSize=${LOAD_SIZE}`);
  }
}

/**
 * 新增任务接口
 * @returns {AxiosPromise<any>}
 */
export function doAddTask(data) {
  return server.post('task/addTask', data, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    processData: false,
  });
}

/**
 * 获取业务类型接口
 * @returns {AxiosPromise<any>}
 */
export function doGetTaskTypeList() {
  return server.get('task/taskType');
}


/**
 * 审核任务接口(演示)
 * @param data
 * @returns {AxiosPromise<any>}
 */
export const doAuditTask = (data) => {
  return server.put('task/auditTaskProcessTemp', data);
};
