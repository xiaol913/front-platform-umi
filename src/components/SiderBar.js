import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';

import styles from './SiderBar.less';

const SubMenu = Menu.SubMenu;

// 侧边栏图层
@withRouter
class SiderBar extends Component {

  constructor(props) {
    super(props);
    const { location } = props;
    let key = location.pathname.split('/')[1] === '' ? 'main' : location.pathname.split('/')[1];
    if (key === 'form') {
      let secKey = location.pathname.split('/')[1] === '' ? 'main' : location.pathname.split('/')[2];
      this.state = {
        defaultOpenKeys: [key],
        defaultSelectedKey: secKey,
      };
    } else {
      this.state = {
        defaultOpenKeys: [],
        defaultSelectedKey: key,
      };
    }
  }

  changeMenu = (e) => {
    this.setState({
      defaultSelectedKey: e.key,
    });
  };

  render() {
    const { showLogo } = this.props;
    return (
      <div>
        {showLogo &&
        <div className={styles.logo_container}>
          <div className={styles.logo}/>
        </div>
        }
        <Menu
          onClick={this.changeMenu}
          defaultSelectedKeys={[this.state.defaultSelectedKey]}
          defaultOpenKeys={this.state.defaultOpenKeys}
          mode="inline"
          theme="dark"
        >
          <Menu.Item key="main">
            <Link to='/'>
              <Icon type="home"/>
              <span>主页</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="task">
            <Link to='/task'>
              <Icon type="file-done"/>
              <span>任务管理</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="record">
            <Link to={'/record'}>
              <Icon type="phone"/>
              <span>录音管理</span>
            </Link>
          </Menu.Item>

          {/*<Menu.Item key="flow">*/}
          {/*<Link to={'/flow'}>*/}
          {/*<Icon type="retweet"/>*/}
          {/*<span>流程管理</span>*/}
          {/*</Link>*/}
          {/*</Menu.Item>*/}

          <SubMenu key="form" title={<span><Icon type="table"/><span>统计报表</span></span>}>
            <Menu.Item key="summary">
              <Link to={'/form/summary'}>
                <Icon type="profile"/>
                <span>汇总报表</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="business">
              <Link to={'/form/business'}>
                <Icon type="layout"/>
                <span>业务报表</span>
              </Link>
            </Menu.Item>
          </SubMenu>

          <Menu.Item key="role">
            <Link to={'/role'}>
              <Icon type="idcard"/>
              <span>角色管理</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="member">
            <Link to={'/member'}>
              <Icon type="team"/>
              <span>用户管理</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="user">
            <Link to={'/user'}>
              <Icon type="setting"/>
              <span>个人设置</span>
            </Link>
          </Menu.Item>

        </Menu>
      </div>
    );
  }
}

export default SiderBar;
