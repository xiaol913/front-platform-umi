import React from 'react';
import { Table, Spin } from 'antd';

import { PAGE_SIZE, PAGE_NUM } from '@/utils/constant';
import styles from './Tables.less';

export function createTable(columns, data, id, loading, size = 'middle') {

  return (
    <Spin
      tip="正在加载..."
      spinning={loading}
    >
      <Table
        className={styles.main}
        size={size}
        columns={columns}
        dataSource={data}
        pagination={{ defaultPageSize: PAGE_SIZE, defaultCurrent: PAGE_NUM, hideOnSinglePage: true }}
        rowKey={record => record[id]}
      />
    </Spin>
  );
}
