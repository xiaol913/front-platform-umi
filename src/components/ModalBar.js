import React from 'react';
import { Button, Modal } from 'antd';

export default (props) => {
  const { loading, title, backText, text, handleOk } = props;

  return <Modal
    title={title}
    visible={loading}
    centered
    onCancel={handleOk}
    footer={[
      <Button
        onClick={handleOk}
        key={'1'}
      >
        {backText}
      </Button>,
    ]}
  >
    <p>{text}</p>
  </Modal>;
}
