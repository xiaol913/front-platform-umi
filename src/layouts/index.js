import React from 'react';
import { LocaleProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';
import 'moment/locale/zh-cn';

import LoginLayout from './LoginLayout';
import BaseLayout from './BaseLayout';

moment.locale('zh-cn');

// 默认图层
export default function(props) {

  const { children } = props;

  if (props.location.pathname === '/login') {
    return <LocaleProvider locale={zh_CN}>
      <LoginLayout>{children}</LoginLayout>
    </LocaleProvider>;
  }

  return <LocaleProvider locale={zh_CN}>
    <BaseLayout>
      {children}
    </BaseLayout>
  </LocaleProvider>;
}
