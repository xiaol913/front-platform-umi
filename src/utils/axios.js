import axios from 'axios';

import { BASE_URL } from '@/utils/constant';
import { responseCodePush } from '@/utils/common';

axios.defaults.withCredentials = true; // 设置默认添加cookie
/**
 * axios实例
 * @type {AxiosInstance}
 */
export const server = axios.create({
  baseURL: BASE_URL,
  // timeout: 5000,
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
  },
});

/**
 * request拦截器
 */
server.interceptors.request.use(
  config => {
    if (config.method === 'post') {
      console.log('正在发送POST请求...');
    }
    if (config.method === 'get') {
      console.log('正在发送GET请求...');
      console.log(config.url);
    }
    return config;
  },
  error => {
    return Promise.reject(error.data.error.message);
  },
);

/**
 * response拦截器
 */
server.interceptors.response.use(
  res => {
    if (res.data.code !== 200 && res.data.code !== 201) {
      responseCodePush(res.data.code);
    }
    return res;
  },
  error => {
    console.log(error);
    responseCodePush(500);
    let errorInfo = error.data.error ? error.data.error.message : error.data;
    return Promise.reject(errorInfo);
  },
);
