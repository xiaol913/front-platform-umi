import Moment from 'moment';
import router from 'umi/router';

/**
 * 格式化时间戳
 * @param timestamp
 * @returns {string}
 */
export function getDateTime(timestamp) {
  return Moment(new Date(timestamp)).format('YYYY-MM-DD HH:mm:ss');
}

/**
 * 判断object是否为空
 * @param data
 * @returns {boolean}
 */
export function isEmptyObject(data) {
  for (let i in data) {
    return true;
  }
  return false;
}

/**
 * 错误码跳转
 * @param code
 */
export function responseCodePush(code) {
  switch (code) {
    case 401:
    case 403:
      router.push('/403');
      break;
    case 404:
      router.push('/404');
      break;
    case 500:
      router.push('/500');
      break;
  }
}
