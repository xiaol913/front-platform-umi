import { message } from 'antd';

import { doGetSummaryTaskList, doGetSummaryCallList } from '@/servers/formApi';

// 用户操作
export default {
  namespace: 'formActions',
  state: {
    summaryTaskList: [],
    summaryCallList: [],
  },
  reducers: {
    showSummaryForm(state, { summaryTaskList, summaryCallList }) {
      return { ...state, summaryTaskList, summaryCallList };
    },
    showSummaryTaskForm(state, { summaryTaskList }) {
      return { ...state, summaryTaskList };
    },
    showSummaryCallForm(state, { summaryCallList }) {
      return { ...state, summaryCallList };
    },
  },
  effects: {
    * getSummaryForm({ data }, { call, put }) {
      if (data) {
        let startTime = data.dateString.length > 0 ? data.dateString[0] : '';
        let endTime = data.dateString.length > 1 ? data.dateString[1] : '';
        switch (data.formType) {
          case 'task':
            const task_resp = yield call(doGetSummaryTaskList, {
              startTime,
              endTime,
              taskName: data.searchValue,
            });
            if (task_resp.data.success) {
              yield put({
                type: 'showSummaryTaskForm',
                summaryTaskList: task_resp.data.data.list,
              });
            } else {
              message.error(task_resp.data.msg);
            }
            break;
          case 'call':
            const call_resp = yield call(doGetSummaryCallList, {
              startTime,
              endTime,
            });
            if (call_resp.data.success) {
              yield put({
                type: 'showSummaryCallForm',
                summaryCallList: call_resp.data.data.list,
              });
            } else {
              message.error(call_resp.data.msg);
            }
            break;
          default:
            break;
        }
      } else {
        const task_resp = yield call(doGetSummaryTaskList, {});
        const call_resp = yield call(doGetSummaryCallList, {});
        if (task_resp.data.success) {
          if (call_resp.data.success) {
            yield put({
              type: 'showSummaryForm',
              summaryTaskList: task_resp.data.data.list,
              summaryCallList: call_resp.data.data.list,
            });
          } else {
            message.error(call_resp.data.msg);
          }
        } else {
          message.error(task_resp.data.msg);
        }
      }
    },
  },
  subscriptions: {},
};
