import { message } from 'antd';

import { doLogin, doUpdatePwd, doGetCaptcha } from '@/servers/userApi';

// 用户操作
export default {
  namespace: 'userActions',
  state: {
    isLogin: false,
    showModal: false,
    captcha: undefined,
  },
  reducers: {
    userAction(state, { isLogin, showModal, captcha }) {
      return { ...state, isLogin, showModal, captcha };
    },
  },
  effects: {
    // 登录
    * login({ data }, { call, put }) {
      const resp = yield call(doLogin, data);
      if (resp.data.success) {
        sessionStorage.setItem('isLogin', 'true');
        sessionStorage.setItem('operId', resp.data.data.operId);
        sessionStorage.setItem('account', resp.data.data.account);
        sessionStorage.setItem('operName', resp.data.data.operName);
        sessionStorage.setItem('createTime', resp.data.data.createTime);
        sessionStorage.setItem('resources', resp.data.data.resources);
        yield put({
          type: 'userAction',
          isLogin: true,
        });
        message.success(resp.data.msg);
      } else {
        message.error(resp.data.msg);
      }
    },
    // 登出
    logout() {
      sessionStorage.clear();
      message.success('登出成功');
    },
    // 修改密码
    * updatePwd({ data }, { call, put }) {
      const resp = yield call(doUpdatePwd, data);
      if (resp.data.success) {
        yield put({
          type: 'userAction',
        });
        message.success(resp.data.msg);
      } else {
        message.error(resp.data.msg);
      }
    },
    // modal开关
    * changeStatus({ data }, { put }) {
      yield put({
        type: 'userAction',
        showModal: !data,
      });
    },
    // 获取验证码
    * getCaptcha(_, { call, put }) {
      const resp = yield call(doGetCaptcha);
      let imgUrl = 'data:image/png;base64,' + btoa(new Uint8Array(resp.data).reduce((data, byte) => data + String.fromCharCode(byte), ''));
      yield put({
        type: 'userAction',
        captcha: imgUrl,
      });
    },
  },
  subscriptions: {},
};
