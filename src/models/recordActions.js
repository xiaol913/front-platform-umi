import { message } from 'antd';

import { doGetRecordList } from '@/servers/recordApi';

export default {
  namespace: 'recordActions',
  state: {
    recordList: [],
  },
  reducers: {
    showRecordList(state, { recordList }) {
      return { ...state, recordList };
    },
  },
  effects: {
    * getRecordList({data}, { call, put }) {
      const resp = yield call(doGetRecordList, data);
      if (resp.data.success) {
        yield put({
          type: 'showRecordList',
          recordList: resp.data.data.list,
        });
      } else {
        message.error(resp.data.msg);
      }
    },
  },
  subscriptions: {},
};
