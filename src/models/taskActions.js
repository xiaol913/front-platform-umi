import { message } from 'antd';
import pathToRegexp from 'path-to-regexp';

import { doAddTask, doGetTaskTypeList, doGetTaskList, doAuditTask } from '@/servers/taskApi';

// 任务操作
export default {
  namespace: 'taskActions',
  state: {
    showModal: false,
    taskTypeList: [],
    taskList: [],
    totalCount: 0,
    auditStatus: '',
  },
  reducers: {
    // 添加任务页面
    addPage(state, { taskTypeList, showModal }) {
      return { ...state, taskTypeList, showModal };
    },
    // 任务列表页面
    showTaskList(state, { taskList, totalCount, totalCompleted, auditStatus }) {
      return { ...state, taskList, totalCount, totalCompleted, auditStatus };
    },
  },
  effects: {
    // 获取任务列表
    * getTaskList({ data }, { call, put, select }) {
      const resp = yield call(doGetTaskList, data);
      if (resp.data.success) {
        let totalCount;
        let totalCompleted;
        if (data) {
          totalCount = yield select(state => state.taskActions.totalCount);
          totalCompleted = yield select(state => state.taskActions.totalCompleted);
        } else {
          totalCount = resp.data.data.totalCount;
          totalCompleted = resp.data.data.list.filter(item => item.auditStatus === 3).length;
        }
        yield put({
          type: 'showTaskList',
          taskList: resp.data.data.list,
          totalCount,
          totalCompleted,
          auditStatus: data,
        });
      } else {
        message.error(resp.data.msg);
      }
    },
    // 增加任务
    * addTask({ data }, { call, put }) {
      const formData = new FormData();
      formData.append('roll', data.roll);
      formData.append('taskType', data.taskType);
      formData.append('taskName', data.taskName);
      const resp = yield call(doAddTask, formData);
      if (resp.data.success) {
        yield put({
          type: 'addPage',
          showModal: true,
        });
      } else {
        message.error(resp.data.msg);
      }
    },
    // add页面加载
    * loadAddPage(_, { call, put }) {
      const resp = yield call(doGetTaskTypeList);
      if (resp.data.success) {
        yield put({
          type: 'addPage',
          taskTypeList: resp.data.data,
        });
      } else {
        message.error(resp.data.msg);
      }
    },
    // 关闭提示框
    * closeModel(_, { put }) {
      yield put({
        type: 'addPage',
        showModal: false,
        loading: false,
      });
    },
    // 模拟审核任务
    * auditTask({ data }, { call, put, select }) {
      const auditStatus = yield select(state => state.taskActions.auditStatus);
      const resp = yield call(doAuditTask, data);
      if (resp.data.success) {
        const res = yield call(doGetTaskList, auditStatus);
        if (res.data.success) {
          let totalCompleted = res.data.data.list.filter(item => item.auditStatus === 3).length;
          yield put({
            type: 'showTaskList',
            taskList: res.data.data.list,
            totalCount: res.data.data.totalCount,
            totalCompleted,
            auditStatus,
          });
        } else {
          message.error(res.data.msg);
        }
      } else {
        message.error(resp.data.msg);
      }
    },
  },
  subscriptions: {},
};
